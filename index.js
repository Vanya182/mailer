/**
 * Vendor libs
 */
const XlsxStreamReader = require("xlsx-stream-reader");
const fs = require('fs');
const Pool = require('threads').Pool;
const pool = new Pool();
const colors = require('colors');
const Spinner = require('cli-spinner').Spinner;
var spinner = new Spinner('Sending.. %s');
spinner.setSpinnerString('|/-\\');
spinner.start();

/**
 * Clear logs files
 */
fs.writeFile('log.txt', '', function(){});
fs.writeFile('error.txt', '', function(){});

/**
 * Get sending params
 */
const fromEmail = process.argv[2] || null;
const sendgridApiKey = process.argv[3] || null;
const usersTable = process.argv[4] || null;



function sendEmail(config) {
    let __dirname = config.__dirname;
    let email = config.currentEmailsPool
    
    const nodemailer = require("nodemailer");
    const sgTransport = require('nodemailer-sendgrid-transport');
    const EmailTemplates = require('swig-email-templates');
    const path = require('path');
    const fs = require('fs');
    

    // FOR SENDGRID
    const options = {
        auth: {
            api_key: config.sendgridApiKey
        }
    };
    const mailer = nodemailer.createTransport(sgTransport(options));

    return new Promise((resolve, reject) => {

        var templates = new EmailTemplates({
            text: false,       // Disable text alternatives
            swig: {
              cache: false     // Don't cache swig templates
            },
            filters: {
              upper: function(str) {
                return str.toUpperCase();
              }
            },
            juice: {
              webResources: {
                images: false      // Inline images under 8kB
              }
            }
        });
        var context = {

        };
        var pathTemplate = path.join(__dirname, 'templates', 'template.html');
        templates.render(pathTemplate, context, function (err, html, text, subject) {
            let userData = {
                email: email
            };

            let mail = {
                to: email,
                from: config['fromEmail'],
                subject: 'Reliable cloud mining with UCMG',
                html: html
            };

            mailer.sendMail(mail, function (error, response) {
                if (error) {
                    fs.appendFile('error.txt', error + '\r\n', err => {
                        if (err) throw err;
                    });
                    console.log(error.underline.red);
                    reject(error);
                }

                fs.appendFile('log.txt', JSON.stringify(response) + '\r\n', err => {
                    if (err) throw err;
                });
                resolve();
            });

        });
    });
}

pool.run(sendEmail);

var workBookReader = new XlsxStreamReader();
workBookReader.on('error', function (error) {
    spinner.stop();
    fs.appendFile('error.txt', error + '\r\n', err => {
        if (err) throw err;
    });
    console.log(error.underline.red);
    throw (error);
});


let currentEmailsPool = [];
workBookReader.on('worksheet', function (workSheetReader) {
    if (workSheetReader.id > 1) {
        // we only want first sheet
        workSheetReader.skip();
        return;
    }

    // if we do not listen for rows we will only get end event
    // and have infor about the sheet like row count
    let emailIndex = null;
    let rowsCount = 0;

    workSheetReader.on('row', function (row) {
        rowsCount += 1;
        if (row.attributes.r == 1) {
            for (let i = 0; i <= row.values.length; i++) {
                if (
                    (typeof row.values[i] == 'string') &&
                    (row.values[i].toLowerCase() == 'email')
                ) {
                    emailIndex = i;
                }

            }
        } else {
            if (emailIndex && row.values[emailIndex]) {
                let email = row.values[emailIndex];
                if (currentEmailsPool.length < 50) {
                    currentEmailsPool.push(email);
                } else {
                    pool.send({
                        __dirname,
                        currentEmailsPool,
                        sendgridApiKey,
                        fromEmail
                    });
                    currentEmailsPool = [];
                }
                // pool.send(__dirname, email);
            }
        }
    });

    workSheetReader.on('end', function () {
        pool.send({
            __dirname, 
            currentEmailsPool,
            sendgridApiKey,
            fromEmail
        });
    });

    pool.on('finished', function (job, message) {
        fs.appendFile('log.txt', 'All emails were send. Thread pool is clear. \r\n', err => {
            if (err) throw err;
        });
        pool.killAll();
        console.log('All mails were sent'.rainbow);
        spinner.stop();
        process.exit();
    }).on('error', function (job, error) {
        spinner.stop();
        console.error(error.underline.red);
        process.exit();
    });

    // call process after registering handlers
    workSheetReader.process();
});

workBookReader.on('end', function () {
    console.log('All datasheet rows were processed'.green);
    fs.appendFile('log.txt', 'All rows were parsed. \r\n', err => {
        if (err) throw err;
    });
});


/**
 * Init app
 */
if (! sendgridApiKey || ! usersTable || ! fromEmail) {
    console.log('Sendgrid API_KEY or Users datasheet or sender email param is missing. Please pass them.'.underline.red);
    spinner.stop();
    process.exit();
}

if (sendgridApiKey && usersTable) {
    console.log('App started. Parsing datasheet'.green);
    fs.createReadStream(usersTable).pipe(workBookReader);
}